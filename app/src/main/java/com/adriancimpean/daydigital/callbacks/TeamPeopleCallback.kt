package com.adriancimpean.daydigital.callbacks

import com.adriancimpean.daydigital.models.User

interface TeamPeopleCallback {
    fun onDataArrived(value : ArrayList<User>)
}