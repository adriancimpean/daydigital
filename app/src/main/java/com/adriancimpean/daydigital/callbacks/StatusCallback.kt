package com.adriancimpean.daydigital.callbacks

interface StatusCallback {
    fun onDataArrived(value : String)
}