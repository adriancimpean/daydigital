package com.adriancimpean.daydigital.callbacks

interface AvatarCallback {
    fun onDataArrived(value : String)
}