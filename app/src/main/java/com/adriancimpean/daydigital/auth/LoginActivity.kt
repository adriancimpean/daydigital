package com.adriancimpean.daydigital.auth

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.BoringLayout
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.adriancimpean.daydigital.MainActivity
import com.adriancimpean.daydigital.R
import com.adriancimpean.daydigital.models.User
import com.adriancimpean.daydigital.repository.LoginRepository
import com.adriancimpean.daydigital.utils.CurrentUser
import com.google.firebase.database.*



class LoginActivity : AppCompatActivity() {
    private val TAG = "LoginActivity"

    private lateinit var loginBtn : Button
    private lateinit var emailEditText : EditText
    private lateinit var passwordEditText: EditText
    private val loginRepository : LoginRepository = LoginRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginBtn = findViewById(R.id.loginBtn)
        emailEditText = findViewById(R.id.emailEditText)
        passwordEditText = findViewById(R.id.passwordEditText)

        loginBtn.setOnClickListener {
            if(validateInput()) {
                loginRepository.login(emailEditText.text.toString(), passwordEditText.text.toString(), this@LoginActivity)
            }
        }
    }

    private fun validateInput () : Boolean {
        if(emailEditText.text.toString() == "") {
            Toast.makeText(this@LoginActivity, "Please enter your email", Toast.LENGTH_SHORT).show()
            return false
        }

        if(passwordEditText.text.toString() == "") {
            Toast.makeText(this@LoginActivity, "Please enter your password", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }
}