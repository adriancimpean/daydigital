package com.adriancimpean.daydigital.auth

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.adriancimpean.daydigital.MainActivity
import com.adriancimpean.daydigital.R
import com.adriancimpean.daydigital.utils.CurrentUser

class SplashScreenActivity : AppCompatActivity() {
    private lateinit var sharedPreferences : SharedPreferences
    private val sharedPrefsLogin = "SP_LOGIN"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        sharedPreferences=getSharedPreferences(sharedPrefsLogin, Context.MODE_PRIVATE)

        if(sharedPreferences.contains("EMAIL") && sharedPreferences.contains("PASSWORD")) {
            CurrentUser.setData(sharedPreferences.getString("FULL_NAME", ""), sharedPreferences.getString("EMAIL", ""), sharedPreferences.getString("PHONE_NUMBER", ""), sharedPreferences.getString("TEAM", ""))
            println("CUURRR USER ${CurrentUser.team}")

            val mainActivity : Intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
            startActivity(mainActivity)
        } else {
            val loginActivity : Intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
            startActivity(loginActivity)
        }
    }
}