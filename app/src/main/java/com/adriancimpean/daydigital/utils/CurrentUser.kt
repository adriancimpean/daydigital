package com.adriancimpean.daydigital.utils

import android.provider.ContactsContract

object CurrentUser {
    var fullName : String? = ""
    var email : String? = ""
    var phoneNumber : String? = ""
    var team : String? = ""

    fun setData (fullName : String?, email : String?, phoneNumber : String?, team : String?) {
        this.fullName = fullName
        this.email = email
        this.phoneNumber = phoneNumber
        this.team = team
    }
}