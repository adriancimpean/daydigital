package com.adriancimpean.daydigital.utils

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.adriancimpean.daydigital.callbacks.AvatarCallback
import com.adriancimpean.daydigital.callbacks.TeamPeopleCallback
import com.adriancimpean.daydigital.models.User
import com.google.firebase.database.*

class Util {
    fun getPeopleFromTeam(team : String?, callback: TeamPeopleCallback) : MutableLiveData<String> {
        val database : DatabaseReference = FirebaseDatabase.getInstance().reference
        val databaseReference : DatabaseReference = database.child("Users")

        var user : User?
        val result : ArrayList<User> = ArrayList()
        val people : MutableLiveData<String> = MutableLiveData()

        val valueEventListener : ValueEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for(ds in snapshot.children) {
                    user = ds.getValue(User::class.java)

                    if(user?.team == team) {
                        user?.let { result.add(it) }
                    }
                }
                callback.onDataArrived(result)

            }
            override fun onCancelled(error: DatabaseError) {
                Log.d("UTIL", error.message)
            }
        }

        databaseReference.addValueEventListener(valueEventListener)
        people.value = result.toString()

        return people
    }

    fun getAvatar(userAssigned : String, callback: AvatarCallback) {
        val database : DatabaseReference = FirebaseDatabase.getInstance().reference
        val databaseReference : DatabaseReference = database.child("Users")

        var user : User?

        val valueEventListener : ValueEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for(ds in snapshot.children) {
                    user = ds.getValue(User::class.java)

                    if(user?.full_name == userAssigned) {
                        val avatar = user?.avatar

                        if (avatar != null) {
                            callback.onDataArrived(avatar)
                        }

                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Log.d("UTIL", error.message)
            }
        }

        databaseReference.addValueEventListener(valueEventListener)
    }
}