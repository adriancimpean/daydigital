package com.adriancimpean.daydigital.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.adriancimpean.daydigital.callbacks.StatusCallback
import com.adriancimpean.daydigital.models.Task
import com.adriancimpean.daydigital.utils.CurrentUser
import com.google.firebase.database.*

class TaskRepository {
    private val database : DatabaseReference = FirebaseDatabase.getInstance().reference
    private val databaseReference : DatabaseReference = database.child("Tasks")

    fun getAssignedTasks() : MutableLiveData<List<Task>> {
        var task : Task? = null

        val result : ArrayList<Task> = ArrayList()
        val liveData : MutableLiveData<List<Task>> = MutableLiveData()

        val valueEventListener : ValueEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for(ds in snapshot.children) {
                    task = ds.getValue(Task::class.java)

                    if(CurrentUser.fullName == task?.Assigned_to){
                        println("TASSKKK ASSIGGNNNEEDD: $task")
                        task?.let { result.add(it) }
                    }
                }
                val sortedResult = result.sortedWith(compareBy(Task::Priority, Task::Title))
                liveData.value = sortedResult
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("TASK REPOSITORY - getTasks", error.message)
            }
        }

        databaseReference.addListenerForSingleValueEvent(valueEventListener)
        return liveData
    }

    fun getCreatedTasks() : MutableLiveData<List<Task>> {
       var task : Task? = null
        val result : ArrayList<Task> = ArrayList()
        val liveData : MutableLiveData<List<Task>> = MutableLiveData()

        val valueEventListener : ValueEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for(ds in snapshot.children) {
                    task = ds.getValue(Task::class.java)

                    if(CurrentUser.fullName == task?.Owner) {
                        task?.let { result.add(it) }
                    }
                }
                val sortedResult = result.sortedWith(compareBy(Task::Priority, Task::Title))
                liveData.value = sortedResult
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("TASK REPOSITORY - getTasks", error.message)
            }
        }

        databaseReference.addListenerForSingleValueEvent(valueEventListener)
        return liveData
    }

    fun addTask(task : Task) {
        databaseReference.push().setValue(task)
    }

    fun updateTaskStatus(newStatus : String, taskTitleToUpdate : String) {
        var query : Query  = databaseReference.orderByChild("title").equalTo(taskTitleToUpdate)

        query.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.d("TASK REPOSITORY", error.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var dataSnapshot : DataSnapshot = snapshot.children.iterator().next()
                var key = dataSnapshot.key

                var path = "/$key"
                var result : HashMap<String, Any> = HashMap()
                result["status"] = newStatus

                databaseReference.child(path).updateChildren(result)
            }
        })
    }

    fun getTaskStatus(taskTitle : String, callback : StatusCallback)  {
        var result : String?

        val valueEventListener : ValueEventListener = object : ValueEventListener {
            var task : Task? = null

            override fun onDataChange(snapshot: DataSnapshot) {
                for(ds in snapshot.children) {
                    task = ds.getValue(Task::class.java)

                    if(taskTitle == task?.Title) {
                        val status = task?.Status

                        if (status != null) {
                            callback.onDataArrived(status)
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("TASK REPOSITORY - getTasks", error.message)
            }
        }
    }
}