package com.adriancimpean.daydigital.repository

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import com.adriancimpean.daydigital.MainActivity
import com.adriancimpean.daydigital.models.User
import com.adriancimpean.daydigital.utils.CurrentUser
import com.google.firebase.database.*

class LoginRepository {
    private val TAG = "LOGIN REPOSITORY"

    fun login(email : String, password : String, activity : Activity) {
        var user : User? = null
        val mActivity = activity

        val database = FirebaseDatabase.getInstance().reference
        val databaseReference : DatabaseReference = database.child("Users")

        val sharedPrefsLogin = "SP_LOGIN"
        var sharedPreferences : SharedPreferences = activity.getSharedPreferences(sharedPrefsLogin, Context.MODE_PRIVATE)

        var loginOk : Boolean = false
        val valueEventListener : ValueEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for(ds in snapshot.children) {
                    user = ds.getValue(User::class.java)
                    if(user?.email == email && user?.password == password) {
                        println("successfully logged in")
                        loginOk = true
                        break;
                    }
                }

                if(loginOk) {
                    //save login data for silent login
                    val editor = sharedPreferences.edit()
                    editor.putString("EMAIL", email)
                    editor.putString("PASSWORD", password)
                    editor.putString("FULL_NAME", user?.full_name)
                    println("========FULL NAME: ${user?.full_name}")
                    editor.putString("PHONE_NUMBER", user?.phone_number)
                    editor.putString("TEAM", user?.team)
                    println("========FULL NAME: ${user?.team}")
                    editor.commit()

                    CurrentUser.setData(user?.full_name, user?.email, user?.phone_number, user?.team)

                    val mainActivity : Intent = Intent(mActivity, MainActivity::class.java)
                    mActivity.startActivity(mainActivity)
                } else {
                    Toast.makeText(mActivity, "Credentials do not match", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d(TAG, error.message)
            }
        }

        databaseReference.addListenerForSingleValueEvent(valueEventListener)
    }
}