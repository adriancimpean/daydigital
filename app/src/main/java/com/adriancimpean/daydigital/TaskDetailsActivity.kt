package com.adriancimpean.daydigital

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.adriancimpean.daydigital.callbacks.AvatarCallback
import com.adriancimpean.daydigital.callbacks.StatusCallback
import com.adriancimpean.daydigital.repository.TaskRepository
import com.adriancimpean.daydigital.utils.Util
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_task_details.*

class TaskDetailsActivity : AppCompatActivity() {
    private lateinit var backBtn : ImageButton
    private lateinit var title : TextView
    private lateinit var description : TextView
    private lateinit var avatar : ImageView
    private lateinit var bottomNav : BottomNavigationView
    private lateinit var status : ImageView

    private var util : Util = Util()
    private var taskRepo : TaskRepository = TaskRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_details)
        backBtn = findViewById(R.id.backBtn)
        title = findViewById(R.id.titleTextView)
        description = findViewById(R.id.descriptionTextView)
        avatar = findViewById(R.id.assignedAvatar)
        status = findViewById(R.id.taskStatus)
        bottomNav = findViewById(R.id.bottomNav)

        title.text = intent.getStringExtra("Title")
        description.text = intent.getStringExtra("Description")

        if(intent.getStringExtra("Status") == "Will do") {
            taskStatus.visibility = View.VISIBLE
        }

        intent.getStringExtra("Avatar")?.let {
            util.getAvatar(it, object : AvatarCallback {
                override fun onDataArrived(value: String) {
                    println("AVATAR: $value")
                    Picasso.get().load(value).into(avatar)
                }
            })
        }

        bottomNav.selectedItemId =R.id.Prios
        bottomNav.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.Prios -> {
                    val priosActivity : Intent = Intent(this@TaskDetailsActivity, MainActivity::class.java)
                    startActivity(priosActivity)
                    overridePendingTransition(0,0)
                }

                R.id.Requests -> {
                    val requestsActivity : Intent = Intent(this@TaskDetailsActivity, RequestsActivity::class.java)
                    startActivity(requestsActivity)
                    overridePendingTransition(0,0)
                }

                R.id.Add -> {
                    val newTaskActivity : Intent = Intent(this@TaskDetailsActivity, NewTaskActivity::class.java)
                    startActivity(newTaskActivity)
                }
            }
            true
        }

        backBtn.setOnClickListener {
            finish()
        }
    }
}