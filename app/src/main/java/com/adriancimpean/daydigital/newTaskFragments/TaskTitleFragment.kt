package com.adriancimpean.daydigital.newTaskFragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.adriancimpean.daydigital.MainActivity
import com.adriancimpean.daydigital.R
import kotlinx.android.synthetic.main.fragment_task_title.*


class TaskTitleFragment : Fragment() {
    private lateinit var closeButton: ImageButton
    private lateinit var nextButton : Button
    private lateinit var taskTitle : EditText
    private lateinit var bundle : Bundle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_task_title, container, false)

        closeButton = view.findViewById(R.id.TitleCloseButton)
        nextButton = view.findViewById(R.id.nextButton)
        taskTitle = view.findViewById(R.id.taskTitleEditText)
        bundle = Bundle()

        nextButton.setOnClickListener {
            bundle.putString("title", taskTitle.text.toString())
            println("TIIIIIIIIIIIIIIIIIIIIIIIIIIIITTTTTTTTTTTTTTTTTLLLLLLLLLLLLLLEEEEEEE ${taskTitle.text.toString()}")
            val taskDescriptionFragment = TaskDescriptionFragment()
            taskDescriptionFragment.arguments = bundle

            if(taskTitle.text.toString() == "") {
                Toast.makeText(context, "Title cannot be blank", Toast.LENGTH_SHORT).show()
            } else {
                val fragmentManager: FragmentManager? = activity?.supportFragmentManager
                val transaction: FragmentTransaction? = fragmentManager?.beginTransaction()
                transaction?.replace(R.id.newTaskContainer, taskDescriptionFragment)
                transaction?.addToBackStack(null)
                transaction?.commit()
            }
        }

        closeButton.setOnClickListener {
            val mainActivity = Intent(activity, MainActivity::class.java)
            startActivity(mainActivity)
        }

        return view
    }
}