package com.adriancimpean.daydigital.newTaskFragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.adriancimpean.daydigital.MainActivity
import com.adriancimpean.daydigital.R


class RequestCreatedFragment : Fragment() {
    private lateinit var backHomeButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_request_created, container, false)

        backHomeButton = view.findViewById(R.id.backHomeButton)

        backHomeButton.setOnClickListener {
            activity?.finish()

            val mainActivity = Intent(context, MainActivity::class.java)
            startActivity(mainActivity)
        }
        return view
    }

}