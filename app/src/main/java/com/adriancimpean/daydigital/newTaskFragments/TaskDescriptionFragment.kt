package com.adriancimpean.daydigital.newTaskFragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.adriancimpean.daydigital.MainActivity
import com.adriancimpean.daydigital.R
import com.adriancimpean.daydigital.callbacks.TeamPeopleCallback
import com.adriancimpean.daydigital.models.Task
import com.adriancimpean.daydigital.models.User
import com.adriancimpean.daydigital.repository.TaskRepository
import com.adriancimpean.daydigital.utils.CurrentUser
import com.adriancimpean.daydigital.utils.Util
import com.squareup.picasso.Picasso


class TaskDescriptionFragment : Fragment() {
    private lateinit var taskTitleTextView: TextView
    private lateinit var taskDescriptionEditText: EditText
    private lateinit var backButton: ImageButton
    private lateinit var closeButton: ImageButton
    private lateinit var doneButton: Button
    private lateinit var topPriorityBtn : Button
    private lateinit var prioBtn : Button
    private lateinit var normalPriorityBtn : Button
    private lateinit var addPeopleBtn : ImageButton
    private lateinit var assignedPersonImageView: ImageView

    var priority : Int = 0
    private var assignedTo : String = ""

    private var taskRepo : TaskRepository = TaskRepository()
    private var util : Util = Util()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_task_description, container, false)
        taskTitleTextView = view.findViewById(R.id.descriptionTaskTitleTextView)
        taskDescriptionEditText = view.findViewById(R.id.taskDescriptionEditText)
        backButton = view.findViewById(R.id.backButton)
        closeButton = view.findViewById(R.id.DescriptionCloseButton)
        doneButton = view.findViewById(R.id.doneButton)
        topPriorityBtn = view.findViewById(R.id.topPriorityButton)
        prioBtn = view.findViewById(R.id.prioButton)
        normalPriorityBtn = view.findViewById(R.id.normalPriorityButton)
        addPeopleBtn = view.findViewById(R.id.addPeopleButton)
        assignedPersonImageView = view.findViewById(R.id.assignedPersonImageView)

        val title = arguments?.getString("title")
        taskTitleTextView.text = title

        println("DESCRIPTION TITLE $title")

        backButton.setOnClickListener {
            val fragmentManager: FragmentManager? = activity?.supportFragmentManager
            val transaction: FragmentTransaction? = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.newTaskContainer, TaskTitleFragment())
            transaction?.addToBackStack(null)
            transaction?.commit()
        }

        closeButton.setOnClickListener {
            val mainActivity = Intent(activity, MainActivity::class.java)
            startActivity(mainActivity)
        }


        /*
            IMPORTANT
            -prioBtn and normalPriorityBtn have an ugly design because the designer didn't finished the background designs for them.

            Only the top prio btn has design for each state(pressed / non-pressed)

            This will be changed as soon as the design will be ready

         */
        
        topPriorityBtn.setOnClickListener {
            if(priority == 0 || priority == 2 || priority == 3) {
                priority = 1
                topPriorityBtn.setBackgroundResource(R.drawable.top_priority)
                topPriorityBtn.setTextColor(Color.parseColor("#FFFFFF"))

                prioBtn.setBackgroundResource(R.drawable.priority_button)
                prioBtn.setTextColor(resources.getColor(R.color.loginBtnColor, null))

                normalPriorityBtn.setBackgroundResource(R.drawable.priority_button)
                normalPriorityBtn.setTextColor(Color.parseColor("#C7C8DB"))
            }
        }

        prioBtn.setOnClickListener {
            if(priority == 0 || priority == 1 || priority == 3) {
                priority = 2
                prioBtn.setBackgroundColor(resources.getColor(R.color.loginBtnColor, null))
                prioBtn.setTextColor(Color.parseColor("#FFFFFF"))

                topPriorityBtn.setBackgroundResource(R.drawable.priority_button)
                topPriorityBtn.setTextColor(Color.parseColor("#FF5071"))
            }

        }

        normalPriorityBtn.setOnClickListener {
            if(priority == 0 || priority == 1 || priority == 2) {
                priority = 3
                normalPriorityBtn.setBackgroundColor(Color.parseColor("#C7C8DB"))
                normalPriorityBtn.setTextColor(Color.parseColor("#FFFFFF"))

                prioBtn.setBackgroundResource(R.drawable.priority_button)
                prioBtn.setTextColor(resources.getColor(R.color.loginBtnColor, null))

                topPriorityBtn.setBackgroundResource(R.drawable.priority_button)
                topPriorityBtn.setTextColor(Color.parseColor("#FF5071"))
            }
        }

        addPeopleBtn.setOnClickListener {

            var users : ArrayList<User>

            var userNames : ArrayList<String> = ArrayList()


            util.getPeopleFromTeam(CurrentUser.team, object : TeamPeopleCallback {
                override fun onDataArrived(value: ArrayList<User>) {
                    users = value

                    for(user in users) {
                        userNames.add(user.full_name)
                    }

                    val builder : AlertDialog.Builder = AlertDialog.Builder(context)
                    println("TEAAM: $users")
                    builder.setItems(userNames.toTypedArray(), object : DialogInterface.OnClickListener{
                        override fun onClick(p0: DialogInterface?, position: Int) {
                            Picasso.get().load(users[position].avatar).into(assignedPersonImageView)
                            assignedTo = users[position].full_name
                            addPeopleBtn.visibility = View.GONE
                            assignedPersonImageView.visibility = View.VISIBLE
                        }
                    })

                    val choosePersonDialog = builder.create()
                    choosePersonDialog.setTitle("Assign Person")
                    choosePersonDialog.show()
                }
            })
        }

        doneButton.setOnClickListener {
            val description = taskDescriptionEditText.text.toString()
            val owner = CurrentUser.fullName
            val status = "none"


            val task = Task(title.toString(), description, assignedTo, priority, owner.toString(), status)

            if (task != null) {
                taskRepo.addTask(task)
            }

            if(assignedTo == "") {
                Toast.makeText(context, "Please assign the task to someone :)", Toast.LENGTH_SHORT).show()
            } else if(priority == 0){
                Toast.makeText(context, "Please set the priority of the task", Toast.LENGTH_SHORT).show()
            } else {
                val fragmentManager: FragmentManager? = activity?.supportFragmentManager
                val transaction: FragmentTransaction? = fragmentManager?.beginTransaction()
                transaction?.replace(R.id.newTaskContainer, RequestCreatedFragment())
                transaction?.addToBackStack(null)
                transaction?.commit()
            }
        }

        return view
    }

}