package com.adriancimpean.daydigital.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.adriancimpean.daydigital.R
import com.adriancimpean.daydigital.TaskDetailsActivity
import com.adriancimpean.daydigital.callbacks.AvatarCallback
import com.adriancimpean.daydigital.callbacks.StatusCallback
import com.adriancimpean.daydigital.models.Task
import com.adriancimpean.daydigital.repository.TaskRepository
import com.adriancimpean.daydigital.utils.Util
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycleview_list_item.view.*

class TasksScreenAdapter (context : Context) : RecyclerView.Adapter<TasksScreenAdapter.ViewHolder> () {
    private var tasks : List<Task> = ArrayList()
    private var mContext = context
    private var util : Util = Util()
    private var taskRepo : TaskRepository = TaskRepository()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(mContext).inflate(R.layout.recycleview_list_item, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentTask = tasks[position]
        holder.taskTitle.text = currentTask.Title
        holder.taskDescription.text=currentTask.Description

        when(currentTask.Priority) {
            1 -> holder.constraintLayout.setBackgroundResource(R.drawable.mask)
            2 -> holder.constraintLayout.setBackgroundResource(R.drawable.mask_prio)
            3 -> holder.constraintLayout.setBackgroundResource(R.drawable.mask_normal)
        }

        util.getAvatar(currentTask.Assigned_to, object : AvatarCallback {
            override fun onDataArrived(value: String) {
                println("AVATAR: $value")
                Picasso.get().load(value).into(holder.avatar)
            }
        })

        if(currentTask.Status == "Will do") {
            holder.itemView.taskStatusImageView.setBackgroundResource(R.drawable.thumb_up)
            holder.itemView.taskStatusImageView.visibility = View.VISIBLE
        }

        holder.itemView.setOnClickListener {
            val taskDetailActivity : Intent = Intent(mContext, TaskDetailsActivity::class.java)
            taskDetailActivity.putExtra("Title", currentTask.Title)
            taskDetailActivity.putExtra("Description", currentTask.Description)
            taskDetailActivity.putExtra("Avatar", currentTask.Assigned_to)
            taskDetailActivity.putExtra("Status", currentTask.Status)
            mContext.startActivity(taskDetailActivity)
        }
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    fun setTasks(tasks : List<Task>) {
        this.tasks = tasks
        notifyDataSetChanged()
    }

    fun getTask(position: Int) : Task {
        return this.tasks[position]
    }

    class ViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView) {
        val taskTitle : TextView = itemView.taskTitleTextView
        val taskDescription: TextView = itemView.taskDescriptionTextView
        val constraintLayout : ConstraintLayout = itemView.recycleListItemLayout
        val avatar : ImageView = itemView.avatarImageView
        val statusImage : ImageView = itemView.taskStatusImageView
    }
}