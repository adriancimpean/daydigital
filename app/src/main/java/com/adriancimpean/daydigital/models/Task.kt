package com.adriancimpean.daydigital.models

import java.lang.StringBuilder


data class Task(
    var Title : String = "",
    var Description : String = "",
    var Assigned_to : String = "",
    var Priority : Int = 0,
    var Owner : String = "",
    var Status : String = ""
) {
}