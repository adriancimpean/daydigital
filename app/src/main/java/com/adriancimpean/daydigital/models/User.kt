package com.adriancimpean.daydigital.models

data class User(
    var email : String = "",
    var password : String = "",
    var full_name : String = "",
    var phone_number : String = "",
    var team : String = "",
    var avatar : String = ""
) {}