package com.adriancimpean.daydigital.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.adriancimpean.daydigital.models.Task
import com.adriancimpean.daydigital.repository.TaskRepository

class RequestsScreenViewModel(application: Application) : AndroidViewModel(application) {
    private var taskRepo : TaskRepository = TaskRepository()
    private var createdTasks : MutableLiveData<List<Task>> = taskRepo.getCreatedTasks()

    fun getCreatedTasks() : MutableLiveData<List<Task>> {
        return createdTasks
    }
}