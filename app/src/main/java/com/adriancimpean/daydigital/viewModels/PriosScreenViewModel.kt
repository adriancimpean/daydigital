package com.adriancimpean.daydigital.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.adriancimpean.daydigital.models.Task
import com.adriancimpean.daydigital.repository.TaskRepository

class PriosScreenViewModel(application: Application) : AndroidViewModel(application) {
    private var taskRepo : TaskRepository = TaskRepository()
    private var assignedTasks : MutableLiveData<List<Task>> = taskRepo.getAssignedTasks()

    fun getTasks() : MutableLiveData<List<Task>> {
        return assignedTasks
    }
}