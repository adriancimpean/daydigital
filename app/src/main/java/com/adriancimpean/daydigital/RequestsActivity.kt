package com.adriancimpean.daydigital

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adriancimpean.daydigital.adapters.TasksScreenAdapter
import com.adriancimpean.daydigital.viewModels.RequestsScreenViewModel
import com.adriancimpean.daydigital.models.Task
import com.google.android.material.bottomnavigation.BottomNavigationView

class RequestsActivity : AppCompatActivity() {
    private lateinit var bottomNav : BottomNavigationView
    private lateinit var adapter : TasksScreenAdapter
    private lateinit var requestsRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_requests)
        bottomNav = findViewById(R.id.bottomNav)
        requestsRecyclerView = findViewById(R.id.requestsRecyclerView)
        adapter = TasksScreenAdapter(this@RequestsActivity)

        requestsRecyclerView.adapter = adapter
        requestsRecyclerView.layoutManager = LinearLayoutManager(this)
        requestsRecyclerView.setHasFixedSize(true)

        val model = ViewModelProvider(this@RequestsActivity).get(RequestsScreenViewModel::class.java)

        model.getCreatedTasks().observe(this, Observer<List<Task>> {
            tasks -> adapter.setTasks(tasks)
        })

        bottomNav.selectedItemId =R.id.Requests
        bottomNav.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.Prios -> {
                    val priosActivity : Intent = Intent(this@RequestsActivity, MainActivity::class.java)
                    startActivity(priosActivity)
                    overridePendingTransition(0,0)
                }

                R.id.Add -> {
                    val newTaskActivity : Intent = Intent(this@RequestsActivity, NewTaskActivity::class.java)
                    startActivity(newTaskActivity)
                }
            }
            true
        }
    }
}