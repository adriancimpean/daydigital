package com.adriancimpean.daydigital

import android.os.Bundle
import android.widget.ImageButton
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.adriancimpean.daydigital.newTaskFragments.TaskTitleFragment


class NewTaskActivity : FragmentActivity() {
    private lateinit var closeButton : ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_task)

        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.newTaskContainer, TaskTitleFragment())
        fragmentTransaction.commit()
    }
}