package com.adriancimpean.daydigital

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adriancimpean.daydigital.adapters.TasksScreenAdapter
import com.adriancimpean.daydigital.models.Task
import com.adriancimpean.daydigital.repository.TaskRepository
import com.adriancimpean.daydigital.viewModels.PriosScreenViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.recycleview_list_item.view.*

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: TasksScreenAdapter
    private lateinit var bottomNav : BottomNavigationView
    private lateinit var priosRecyclerView: RecyclerView
    private lateinit var sharedPreferences : SharedPreferences
    private val sharedPrefsLogin = "SP_LOGIN"

    private var taskRepo = TaskRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPreferences = getSharedPreferences(sharedPrefsLogin, Context.MODE_PRIVATE)

        bottomNav = findViewById(R.id.bottomNav)
        priosRecyclerView = findViewById(R.id.priosRecyclerView)

        adapter = TasksScreenAdapter(this@MainActivity)
        priosRecyclerView.adapter = adapter
        priosRecyclerView.layoutManager = LinearLayoutManager(this)
        priosRecyclerView.setHasFixedSize(true)

        val model = ViewModelProvider(this@MainActivity).get(PriosScreenViewModel::class.java)

        model.getTasks().observe(this, Observer<List<Task>> {
                tasks -> adapter.setTasks(tasks)
                adapter.notifyDataSetChanged()
        })

         ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                    viewHolder.itemView.taskStatusImageView.setBackgroundResource(R.drawable.thumb_up)
                    viewHolder.itemView.taskStatusImageView.visibility = View.VISIBLE
                    taskRepo.updateTaskStatus("Will do", adapter.getTask(position).Title)
                    adapter.notifyDataSetChanged()
            }

             override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                 super.onChildDraw(c, recyclerView, viewHolder, dX/4, dY, actionState, isCurrentlyActive)
             }
         }).attachToRecyclerView(priosRecyclerView)

        bottomNav.selectedItemId =R.id.Prios
        bottomNav.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.Requests -> {
                    val requestsActivity : Intent = Intent(this@MainActivity, RequestsActivity::class.java)
                    startActivity(requestsActivity)
                    overridePendingTransition(0,0)
                }

                R.id.Add -> {
                    val newTaskActivity : Intent = Intent(this@MainActivity, NewTaskActivity::class.java)
                    startActivity(newTaskActivity)
                }
            }
            true
        }
    }
}